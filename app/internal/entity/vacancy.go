package entity

import "encoding/json"

func UnmarshalVacancy(data []byte) (Vacancy, error) {
	var r Vacancy
	err := json.Unmarshal(data, &r)
	return r, err
}

func (r *Vacancy) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

type Vacancy struct {
	//Context      string     `json:"@context"`
	//Type         string     `json:"@type"`
	DatePosted   string     `json:"datePosted" db:"date_posted"`
	Title        string     `json:"title" db:"title"`
	Description  string     `json:"description" db:"description"`
	Identifier   Identifier `json:"identifier" db:"identifier"`
	ValidThrough string     `json:"validThrough" db:"valid_through"`
	/*HiringOrganization HiringOrganization `json:"hiringOrganization"`
	JobLocation        []JobLocation      `json:"jobLocation"`
	JobLocationType    string             `json:"jobLocationType"`*/
	EmploymentType string `json:"employmentType" db:"employment_type"`
}

/*type HiringOrganization struct {
	Type   string `json:"@type"`
	Name   string `json:"name"`
	Logo   string `json:"logo"`
	SameAs string `json:"sameAs"`
}*/

type Identifier struct {
	Type  string `json:"@type" db:"type"`
	Name  string `json:"name" db:"name"`
	Value string `json:"value" db:"id"`
}

/*type JobLocation struct {
	Type    string  `json:"@type"`
	Address Address `json:"address"`
}

type Address struct {
	Type            string         `json:"@type"`
	StreetAddress   string         `json:"streetAddress"`
	AddressLocality string         `json:"addressLocality"`
	AddressCountry  AddressCountry `json:"addressCountry"`
}

type AddressCountry struct {
	Type string `json:"@type"`
	Name string `json:"name"`
}
*/
