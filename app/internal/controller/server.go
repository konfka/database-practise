package controller

import (
	"context"
	"fmt"
	"github.com/go-chi/chi"
	"gitlab.com/konfka/go-go-kata/app/config"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"
)

type ServerController struct {
	v    *VacancyController
	conf config.AppConf
}

func NewServerController(v *VacancyController, conf config.AppConf) *ServerController {
	return &ServerController{v: v, conf: conf}
}

func (c *ServerController) Start() {
	r := chi.NewRouter()

	r.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/", http.FileServer(http.Dir("./public"))).ServeHTTP(w, r)
	})

	r.Mount("/swagger", swaggerRouter())
	r.Mount("/vacancies", vacancyRouter(c.v, c))

	port := fmt.Sprintf(":%s", c.conf.Server.Port)

	srv := &http.Server{
		Addr:    port,
		Handler: r,
	}

	// Запуск веб-сервера в отдельном горутине
	go func() {
		log.Printf("server started on port %s\n", c.conf.Server.Port)
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	// Ожидание сигнала для начала завершения работы
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server ...")

	// Установка тайм-аута для завершения работы
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}

	log.Println("Server exiting")
}
