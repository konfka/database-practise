// Package classification infoblog.
//
// Documentation of your project API.
//
//	Schemes:
//	- http
//	- https
//	BasePath: /
//	Version: 1.0.0
//
//	Consumes:
//	- application/json
//	- multipart/form-data
//
//	Produces:
//	- application/json
//
//	Security:
//	- basic
//
//
//	SecurityDefinitions:
//	  Bearer:
//	    type: apiKey
//	    name: Authorization
//	    in: header
//
// swagger:meta
package docs

//go:generate swagger generate spec -o /Users/svecsofa/go/src/gitlab.com/konfka/database-practise/app/internal/public/swagger.json --scan-models
