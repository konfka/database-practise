FROM golang:1.20 AS builder

WORKDIR /build

COPY go.mod go.sum ./

RUN go mod download && go mod verify

COPY . .

RUN go build -o /build/main ./app/cmd/main.go

FROM golang:1.20

WORKDIR /build

COPY --from=builder /build/main /build/main

EXPOSE 8081

CMD ["./main"]