package docs

import (
	"gitlab.com/konfka/go-go-kata/app/internal/entity"
)

//go:generate swagger generate spec -o /Users/svecsofa/go/src/gitlab.com/konfka/database-practise/app/internal/public/swagger.json --scan-models

// swagger:route POST /vacancies/search vacancy vacancySearchRequest
// Парсинг вакансий с habr-а.
// security:
//   - Bearer: []
// parameters:
// + name: query
// in: query
// description: Query string
// required: true
//
// responses:
//  200: description: Shows the number of vacancies found.
//  400: description: Bad request
//	500: description: Internal server error

// swagger:route POST /vacancies/delete vacancy vacancyDeleteRequest
// Удаление вакансии.
// security:
//   - Bearer: []
// parameters:
// + name: id
// in: query
// description: Vacancy ID
// required: true
//
// responses:
//  200:
//  400: description: Bad request
//	500: description: Internal server error

// swagger:route POST /vacancies/get vacancy vacancyGetRequest
// Получение вакансии по id.
// security:
//   - Bearer: []
// parameters:
// + name: id
// in: query
// description: Vacancy ID
// required: true
//
// responses:
//  200: vacancySearchResponse
//  400: description: Bad request
//	500: description: Internal server error

// swagger:response vacancySearchResponse
type vacancyGetResponse struct {
	// in:body
	Body entity.Vacancy
}

// swagger:route Get /vacancies/list vacancy vacancyListRequest
// Все вакансии.
// security:
//   - Bearer: []
// responses:
//  200: vacancyListResponse
//  400: description: Bad request
//	500: description: Internal server error

// swagger:response vacancyListResponse
type vacancyListResponse struct {
	// in:body
	Body []entity.Vacancy
}

// swagger:route PUT /vacancies/update vacancy vacancyUpdateRequest
// Обновление вакансии.
// security:
//   - Bearer: []
// responses:
//  200: vacancyUpdateResponse
//  400: description: Bad request
//	500: description: Internal server error

// swagger:parameters vacancyUpdateRequest
type vacancyUpdateRequest struct {
	// Vacancy object that needs to update
	// required: true
	// in:body
	Body entity.Vacancy
}

// swagger:response vacancyUpdateResponse
type vacancyUpdateResponse struct {
	// in:body
	Body entity.Vacancy
}
