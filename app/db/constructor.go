package db

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	"gitlab.com/konfka/go-go-kata/app/config"
)

func NewSqlDB(db config.DB) (*sqlx.DB, error) {
	var dsn string
	var err error
	var dbRaw *sqlx.DB

	switch db.Driver {
	case "postgres":
		dsn = fmt.Sprintf("host =%s port=%s password=%s dbname=%s sslmode=disable", db.Host, db.Port, db.Password, db.Name)
	default:
		return nil, fmt.Errorf("unsupported database driver: %s", db.Driver)
	}
	dbRaw, err = sqlx.Connect(db.Driver, dsn)
	if err != nil {
		return nil, err
	}
	return dbRaw, nil
}
