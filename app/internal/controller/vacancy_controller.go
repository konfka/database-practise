package controller

import (
	"encoding/json"
	"fmt"
	"gitlab.com/konfka/go-go-kata/app/internal/entity"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
)

type VacancyService interface {
	Search(string) int
	GetByID(int) (entity.Vacancy, error)
	GetList() ([]entity.Vacancy, error)
	Delete(int) error
	Update(vacancy entity.Vacancy) (entity.Vacancy, error)
}

type VacancyController struct {
	storage VacancyService
}

func NewVacancyController(s VacancyService) *VacancyController {
	return &VacancyController{storage: s}
}

func (v *VacancyController) Search(w http.ResponseWriter, r *http.Request) {
	query := r.FormValue("query")

	v.storage.Search(query)

	_, _ = fmt.Fprintf(w, "Parsed successfully\n")
}

func (v *VacancyController) GetByID(w http.ResponseWriter, r *http.Request) {
	idRaw := chi.URLParam(r, "id")
	id, err := strconv.Atoi(idRaw)
	if err != nil { // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
	vacancy, err := v.storage.GetByID(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(vacancy)
	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

}

func (v *VacancyController) GetList(w http.ResponseWriter, r *http.Request) {

	vacancies, err := v.storage.GetList()
	if err != nil { // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(vacancies)
	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

}

func (v *VacancyController) Delete(w http.ResponseWriter, r *http.Request) {

	idRaw := chi.URLParam(r, "id")

	id, err := strconv.Atoi(idRaw)
	if err != nil { // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	err = v.storage.Delete(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	_, _ = fmt.Fprintf(w, "Deleted successfully\n")
}

func (c VacancyController) Update(w http.ResponseWriter, r *http.Request) {
	var vacancy entity.Vacancy

	err := json.NewDecoder(r.Body).Decode(&vacancy) // считываем приходящий json из *http.Request в структуру Pet
	if err != nil {                                 // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
	defer r.Body.Close()

	vacancy, err = c.storage.Update(vacancy)
	if err != nil { // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(vacancy)
	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

}
