package repository

import (
	"context"
	"errors"
	"gitlab.com/konfka/go-go-kata/app/internal/entity"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"strconv"
)

type VacancyMongo struct {
	ID              primitive.ObjectID `bson:"id,omitempty"`
	Title           string             `bson:"title"`
	Description     string             `bson:"description"`
	EmploymentType  string             `bson:"employmentType"`
	DatePosted      string             `bson:"datePosted"`
	ValidThrough    string             `bson:"validThrough"`
	IdentifierType  string             `bson:"identifierType"`
	IdentifierName  string             `bson:"identifierName"`
	IdentifierValue string             `bson:"identifierValue"`
}

type VacancyStorageNoSQL struct {
	collection *mongo.Collection
}

func NewVacancyStorageNoSQL(db *mongo.Client) *VacancyStorageNoSQL {
	return &VacancyStorageNoSQL{collection: db.Database("go-parser").Collection("vacancies")}
}

func (r VacancyStorageNoSQL) Create(v entity.Vacancy) (entity.Vacancy, error) {
	id, err := strconv.ParseInt(v.Identifier.Value, 10, 64)
	if err != nil {
		return entity.Vacancy{}, err
	}

	if _, err = r.Read(id); err == nil {
		return entity.Vacancy{}, errors.New("vacancy id:" + v.Identifier.Value + " exists")
	}

	_, err = r.collection.InsertOne(context.TODO(), r.vacancyToMongo(v))
	if err != nil {
		return entity.Vacancy{}, err
	}

	return v, nil
}

func (r VacancyStorageNoSQL) Read(id int64) (entity.Vacancy, error) {
	var v VacancyMongo

	vID := strconv.FormatInt(id, 10)

	err := r.collection.FindOne(context.TODO(), bson.M{"identifierValue": vID}).Decode(&v)
	if err != nil {
		return entity.Vacancy{}, err
	}

	return r.mongoToVacancy(v), nil
}

func (r VacancyStorageNoSQL) List() []entity.Vacancy {
	var res []entity.Vacancy

	findOptions := options.Find()
	cur, err := r.collection.Find(context.TODO(), bson.D{{}}, findOptions)
	if err != nil {
		return nil
	}

	for cur.Next(context.TODO()) {
		var elem VacancyMongo
		err = cur.Decode(&elem)
		if err != nil {
			log.Fatal(err)
		}

		res = append(res, r.mongoToVacancy(elem))
	}

	return res
}

func (r VacancyStorageNoSQL) Delete(id int64) error {
	vID := strconv.FormatInt(id, 10)
	var err error

	_, err = r.collection.DeleteOne(context.TODO(), bson.M{"identifierValue": vID})
	if err != nil {
		return err
	}
	return err
}

func (r VacancyStorageNoSQL) Update(vacancy entity.Vacancy) (entity.Vacancy, error) {
	id, err := strconv.ParseInt(vacancy.Identifier.Value, 10, 64)
	if err != nil {
		return entity.Vacancy{}, err
	}

	if err = r.Delete(id); err != nil {
		return entity.Vacancy{}, errors.New("id not found")
	}

	return r.Create(vacancy)
}

func (r VacancyStorageNoSQL) vacancyToMongo(v entity.Vacancy) VacancyMongo {
	var res VacancyMongo

	res.Title = v.Title
	res.Description = v.Description
	res.EmploymentType = v.EmploymentType
	res.DatePosted = v.DatePosted
	res.ValidThrough = v.ValidThrough
	res.IdentifierType = v.Identifier.Type
	res.IdentifierName = v.Identifier.Name
	res.IdentifierValue = v.Identifier.Value

	return res
}

func (r VacancyStorageNoSQL) mongoToVacancy(m VacancyMongo) entity.Vacancy {
	var res entity.Vacancy

	res.Title = m.Title
	res.Description = m.Description
	res.EmploymentType = m.EmploymentType
	res.DatePosted = m.DatePosted
	res.ValidThrough = m.ValidThrough
	res.Identifier.Type = m.IdentifierType
	res.Identifier.Name = m.IdentifierName
	res.Identifier.Value = m.IdentifierValue

	return res
}
