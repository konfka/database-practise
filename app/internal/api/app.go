package api

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/konfka/go-go-kata/app/config"
	"gitlab.com/konfka/go-go-kata/app/internal/controller"
	"gitlab.com/konfka/go-go-kata/app/internal/repository"
	"gitlab.com/konfka/go-go-kata/app/internal/service"
)

func Run(conf config.AppConf, db *sqlx.DB) {
	p := controller.NewVacancyPars(conf)

	rSQL := repository.NewPostgresRepository(db)
	vs := service.NewUserService(rSQL, p)

	vc := controller.NewVacancyController(vs)

	srv := controller.NewServerController(vc, conf)

	srv.Start()
}
