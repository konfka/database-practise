package repository

import (
	"errors"
	"fmt"
	"github.com/jmoiron/sqlx"
	"gitlab.com/konfka/go-go-kata/app/internal/entity"
)

type RepositoryStorager interface {
	Create(vacancy entity.Vacancy) error
	GetByID(id int) (*entity.Vacancy, error)
	GetList() ([]entity.Vacancy, error)
	Delete(id int) error
	Update(vacancy entity.Vacancy) (entity.Vacancy, error)
}

// PostgresRepository представляет репозиторий для работы с вакансиями
type PostgresRepository struct {
	db *sqlx.DB
}

// NewPostgresRepository создает новый экземпляр VacancyRepository
func NewPostgresRepository(db *sqlx.DB) *PostgresRepository {
	return &PostgresRepository{
		db: db,
	}
}

func (p *PostgresRepository) Create(vacancy entity.Vacancy) error {
	// SQL-запрос
	query := `INSERT INTO vacancies (id, title, description, date_posted, valid_through, employment_type)
			  VALUES (:identifier.id, :title, :description, :date_posted, :valid_through, :employment_type)`
	// Выполняем запрос
	_, err := p.db.NamedExec(query, vacancy)
	if err != nil {
		return fmt.Errorf("failed to create vacancy: %v", err)
	}

	return nil
}

func (p *PostgresRepository) GetByID(id int) (entity.Vacancy, error) {
	// SQL-запрос
	query := "SELECT * FROM vacancies WHERE id = $1"

	// Выполняем запрос
	var vacancy entity.Vacancy
	err := p.db.Get(&vacancy, query, id)
	if err != nil {
		return entity.Vacancy{}, fmt.Errorf("failed to get vacancy by ID: %v", err)
	}

	return vacancy, nil
}

func (p *PostgresRepository) GetList() ([]entity.Vacancy, error) {
	// sql запрос
	query := "SELECT * FROM vacancies"
	// выполняем запрос
	var vacancies []entity.Vacancy
	err := p.db.Select(&vacancies, query)
	if err != nil {
		return nil, fmt.Errorf("failed to get vacancies list: %v", err)
	}
	return vacancies, nil
}

func (p *PostgresRepository) Delete(id int) error {
	// sql запрос
	query := "DELETE FROM vacancies WHERE id = $1"

	_, err := p.db.Exec(query, id)
	if err != nil {
		return fmt.Errorf("failed to delete vacancy: %v", err)
	}
	return nil
}
func (p *PostgresRepository) Update(vacancy entity.Vacancy) (entity.Vacancy, error) {
	query := `UPDATE vacancies SET title=:title, description=:description, identifier_type=:identifier.type,
                     identifier_name=:identifier.name, employment_type=:employment_type, 
                     job_location=:job_location.address, date_posted=:date_posted, 
                     valid_through=:valid_through 
 			  WHERE id=:identifier.id`

	_, err := p.db.NamedExec(query, vacancy)
	if err != nil {
		return entity.Vacancy{}, errors.New(fmt.Sprintf("id %s, error: %s", vacancy.Identifier.Value, err.Error()))
	}

	return vacancy, nil

}
