-- docker run -e POSTGRES_PASSWORD=password -e POSTGRES_USER=sqlDB -p 5432:5432 -v ./scripts/InitDB:/docker-entrypoint-initdb.d sqlDB
-- CREATE SCHEMA parser;

CREATE TABLE IF NOT EXISTS vacancies
(
    id               bigint not null primary key,
    title            varchar(255),
    description      varchar(12000),
    identifier_type  varchar(55),
    identifier_name  varchar(55),
    employment_type  varchar(55),
    date_posted      date,
    valid_through    date
    );
