package controller

import (
	"github.com/go-chi/chi/v5"
	"net/http"
)

func vacancyRouter(c *VacancyController, s *ServerController) http.Handler {
	r := chi.NewRouter()

	r.Post("/search", c.Search)
	r.Post("/get", c.GetByID)
	r.Post("/delete", c.Delete)
	r.Get("/list", c.GetList)
	r.Put("/update", c.Update)

	return r
}
