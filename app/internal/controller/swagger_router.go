package controller

import (
	"github.com/go-chi/chi"
	"gitlab.com/konfka/go-go-kata/app/internal/templates"
	"net/http"
)

func swaggerRouter() http.Handler {
	r := chi.NewRouter()
	r.Get("/", templates.SwaggerUI)

	return r
}
